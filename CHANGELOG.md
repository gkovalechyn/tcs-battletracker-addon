# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 0.2.2 (2019-07-19)
### Fixed
* Bug with the Deleted event handler because it fires when a unit gets inside a vehicle.

### Added
* Messages to the sender of the begin and end capture functions.

## 0.2.1 (2019-07-08)
### Fixed
* Wrong isNil usage in fn_localEH.
* Readme referenced the wrong function.

## 0.2.0 (2019-07-01)
### Fixed
* Killer and Victim being having the same ID on the Killed event.
* Not handling ACE Medical on the Hit event handler.
* Units were not being properly marked as dead and appeared as alive even though they were dead in the web renderer.
* Player units not being recorded properly due to arma's respawn being retarded.
* Event handlers not firing due to locality issues.

### Changed
* Killed, Hit and fired event handlers are now added where the vehicle is local and then whoever is 
responsible for those units broadcasts back to whoever has the mod the data for that event.
* Event handlers that need to be local were moved into their `add____EH.sqf` files and that file is
to be remoteExec'ed in the proper clients.

### Removed
* fn_initUnit, now all units use fn_initVehicle.

## 0.1.0 - 2019-03-07
### Added
* First release.

### Changed
* Switched from mikero's tools to AddonBuilder.
