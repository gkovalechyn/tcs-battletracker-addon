#include "script_component.hpp"

[
	QUOTE(GVARMAIN(setting_bulletTrackingEnabled)),
	"CHECKBOX",
	"Enable bullet tracking",
	"BattleTracker",
	false //Value info, for CHECKBOX it's the default setting
] call CBA_settings_fnc_init;


[
	QUOTE(GVARMAIN(setting_captureInterval)),
	"SLIDER",
	["Capture interval", "Determines the interval between each snapshot of the captured units"],
	"BattleTracker",
	[1, 10, 2, 0]
] call CBA_settings_fnc_init;


[
	QUOTE(GVARMAIN(setting_automaticCaptureEnabled)),
	"CHECKBOX",
	["Enable automatic mission capture", "Automatically captures all missions. If this is false the begin and end capture functions need to be called to capture the mission."],
	"BattleTracker",
	false //Value info, for CHECKBOX it's the default setting
] call CBA_settings_fnc_init;