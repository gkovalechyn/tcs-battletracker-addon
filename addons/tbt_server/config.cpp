#include "script_component.hpp"

class CfgPatches {
  class ADDON {
    name = QUOTE(COMPONENT);
    units[] = {};
    weapons[] = {};
    requiredVersion = REQUIRED_VERSION;
    requiredAddons[] = {
      "cba_main",
      "cba_xeh",
      "tbt_settings"
    };
    author = "gkovalechyn";
    VERSION_CONFIG;
  };
};

class CfgFunctions {
  class TBT {
    class EventHandlers {
      file = "z\TBT\addons\server\EventHandlers";

      class addFiredEH {};
      class addHitEH {};
      class addKilledEH {};
      class addRespawnedEH {};
      class addLocalEH {};
      
      class deletedEH {};

      class postInit {};
      class preInit {};
      class vehiclePostInit {};
    };

    class Internal {
      file = "z\TBT\addons\server\Internal";

      class constants {};
      class setupVariables {};

      class cleanupVehicle {};
      class getUnitTypeNumber {};
      class initVehicle {};
      class getNextUnitID {};
      class isUnitInitialized {};

      class vehicleWasHit {};
      class vehicleWasKilled {};
      class vehicleFired {};
      class playerRespawned {};
      
      class sendVehicleUpdate {};
      class addEventHandlers {};
      class updateLoop {};
    };

    class Public {
      file = "z\TBT\addons\server";

      class beginCapture {};
      class endCapture {};
    };
  };
};

#include "CfgEventHandlers.cpp"