class Extended_PostInit_EventHandlers {
  class ADDON {
    init = "[] call TBT_fnc_postInit;";
  };
};

class Extended_PreInit_EventHandlers {
    class MissionPreInit {
        init = "[] call TBT_fnc_preInit;";
    };
};

class Extended_InitPost_EventHandlers {
  // As for why these numbers check VehicleType on the Extension
  // Man
  class CAManBase {
      class SoldierInitRespawn {
        init = "(_this + [0]) call TBT_fnc_vehiclePostInit;";
      };
  };

  // Attack or transport helicopter
  class Helicopter {
    class HelicopterInit {
      init = "(_this + [1]) call TBT_fnc_vehiclePostInit;";
    };
  };

  // Planes, UAVs
  class Plane {
    class PlaneInit {
      init = "(_this + [2]) call TBT_fnc_vehiclePostInit;";
    };
  };

  // Cars, trucks, MRAPs
  class Car {
    class CarInit {
      init = "(_this + [3]) call TBT_fnc_vehiclePostInit;";
    };
  };

  // Tanks, APCs
  class Tank {
    class TankInit {
      init = "(_this + [4]) call TBT_fnc_vehiclePostInit;";
    };
  };

  // All ships
  class Ship {
    class ShipInit {
      init = "(_this + [5]) call TBT_fnc_vehiclePostInit;";
    };
  };

  // StaticWeapons, AA, HMGs
  class StaticWeapon {
    class StaticWeaponInit {
      init = "(_this + [6]) call TBT_fnc_vehiclePostInit;";
    };
  };
};