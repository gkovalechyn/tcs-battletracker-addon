#include "script_component.hpp"

{
	GVARMAIN(extensionName) callExtension ["entityDeleted", [
		_x getVariable QGVAR(unitID),
		time
	]];

	[_x] call TBT_fnc_cleanupVehicle;
} forEach GVAR(unitList) + GVAR(deadList);

["ace_unconscious", GVAR(unconsciousEHID)] call CBA_fnc_removeEventHandler;

// Timestamp
GVARMAIN(extensionName) callExtension ["missionEnded", [
	time
]];

// Cleanup variables
GVAR(isCapturing) = false;
GVAR(unitList) = [];
GVAR(deadList) = [];
GVAR(nextUnitID) = 0;

INFO("Finished capturing units.");

if (isRemoteExecuted) then {
	"[TBT] Stopped recording." remoteExec ["systemChat", remoteExecutedOwner];
};
