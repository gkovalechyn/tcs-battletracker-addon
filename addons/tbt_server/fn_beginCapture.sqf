#include "script_component.hpp"

if (GVAR(isCapturing)) exitWith {};

GVAR(unconsciousEHID) = ["ace_unconscious", {
	params ["_unit", "_isUnconscious"];

	[_unit] call TBT_fnc_sendVehicleUpdate;
}] call CBA_fnc_addEventHandler;

private _worldsize = getnumber (configfile >> "CfgWorlds" >> worldName >> "mapSize");

// WorldName, worldSize, missionName, timestamp
GVARMAIN(extensionName) callExtension ["missionStarted", [
	worldName,
	[_worldsize, _worldsize],
	briefingName,
	time
]];

GVAR(isCapturing) = true;

// Initialize all the units that weren't initialize because we weren't gathering data when the mission started.
{
	private _unit = _x;
	private _unitType = [_unit] call TBT_fnc_getUnitTypeNumber;

	if (_unitType >= 0) then {
		[_unit, _unitType] call TBT_fnc_initVehicle;
	};
} forEach (allUnits + vehicles);

[] spawn TBT_fnc_updateLoop;

INFO("Started capturing units.");

if (isRemoteExecuted) then {
	"[TBT] Started recording." remoteExec ["systemChat", remoteExecutedOwner];
};