#include "script_component.hpp"

/*
	Description:
		Adds a killed event handler to the given vehicle where the vehicle is local that sends back the results of the event
		to whoever called the function. It also supports ace medical by trying to get "ace_medical_lastDamageSource" object 
		if it is available.

	Parameters:
		0 - (vehicle) The object the event handler should be added to.
		1 - (netID) The ID of whoever the event should be broadcast back to.

	Returns:
		None

	Example:
		[[_vehicle, clientOwner], TBT_fnc_addRespawnedEH] remoteExec ["call", owner _vehicle];
*/

private _eventHandlerCode = {
	params ["_unit", "_corpse"];
	private _owner = _corpse getVariable QGVAR(respawnEventHandlerOwner);

	[
		_corpse call BIS_fnc_netId,
		_unit call BIS_fnc_netId
	] remoteExec ["TBT_fnc_playerRespawned", _owner];
};

params ["_vehicle", "_owner"];

private _handlerID = _vehicle getVariable QGVAR(respawnEventHandlerID);

if (isNil "_handlerID") then {
	_id = player addEventHandler ["Respawn", _eventHandlerCode];

	_vehicle setVariable [QGVAR(respawnEventHandlerID), _id];
	_vehicle setVariable [QGVAR(respawnEventHandlerOwner), _owner];
};
