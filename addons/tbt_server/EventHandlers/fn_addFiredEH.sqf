#include "script_component.hpp"

/*
	Description:
		Fired event handler for a vehicle. To be added to the owning computer of the vehicle through remoteExec.

	Parameters:
		0 - (vehicle) The object the event handler should be added to.
		1 - (netID) The ID of whoever the event should be broadcast back to.

	Returns:
		None

	Example:
		[[_vehicle, clientOwner], TBT_fnc_addFiredEH] remoteExec ["call", owner _vehicle];

*/
private _eventHandlerCode = {
	params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];
	private _unitID = _unit call BIS_fnc_netId;
	private _firedPos = getPos _unit;
	private _firedTime = time;
	private _owner = _unit getVariable QGVAR(firedEventHandlerOwner);

	[_unitID, _firedPos, _firedTime, _projectile, _owner] spawn {
		params ["_unitID", "_firedPos", "_firedTime", "_projectile", "_owner"];
		private _lastProjectilePos = _firedPos;

		while {!isNull _projectile} do {
			_lastProjectilePos = getPos _projectile;
			sleep 0.01;
		};

		[
			_unitID,
			_firedPos,
			_firedTime,
			_lastProjectilePos
		] remoteExec ["TBT_fnc_vehicleFired", _owner];
	};
};
params ["_vehicle", "_owner"];

private _handlerID = _vehicle getVariable QGVAR(firedEventHandlerID);

if (isNil "_handlerID") then {
	private _id = _vehicle addEventHandler ["Fired", _eventHandlerCode];

	_vehicle setVariable [QGVAR(firedEventHandlerID), _id];
	_vehicle setVariable [QGVAR(firedEventHandlerOwner), _owner];
	_vehicle setVariable [QGVAR(firedEventHandlerCode), _eventHandlerCode];
};

