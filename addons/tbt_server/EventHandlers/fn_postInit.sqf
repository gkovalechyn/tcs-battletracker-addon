#include "script_component.hpp"

addMissionEventHandler ["Ended", {
	if (GVAR(isCapturing)) then {
		[] call TBT_fnc_endCapture;
	};
}];

if (hasInterface) then {
	player createDiarySubject ["BattleTracker", "BattleTracker"];
	player createDiaryRecord [
		"BattleTracker",
		[
			"Controls",
			"
			<execute expression=""
				if (TBT_server_isCapturing) then {
					hint 'There is already a recording in progress';
				} else {
					[] call TBT_fnc_beginCapture;
					hint 'Started capturing';
				};
			"">Start recording</execute><br/>

			<execute expression=""
				if (!TBT_server_isCapturing) then {
					hint 'There is no capture in progress';
				} else {
					[] call TBT_fnc_endCapture;
					hint 'Stopped capturing';
				};
			"">Stop recording</execute><br/>
			"
		]
	];
};