#include "script_component.hpp"
ADDON = false;

[] call TBT_fnc_setupVariables;

if (GVARMAIN(setting_automaticCaptureEnabled)) then {
	[] call TBT_fnc_beginCapture;
};

ADDON = true;