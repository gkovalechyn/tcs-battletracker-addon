#include "script_component.hpp"

/*
	Description:
		Post init handler of vehicles.
	
	Parameters:
		0 - (object) The unit that needs to be initialized.

	Returns:
		None

*/

params ["_unit", "_vehicleType"];




if (!GVAR(isCapturing)) exitWith {
	TRACE_2("POST INIT - Not capturing, unit not initialized.", _unit, _vehicleType);
};

if ([_unit] call TBT_fnc_isUnitInitialized) exitWith {
	WARNING_2("XEH_initPostVehicle - Tried to initialize unit %1(%2) twice", _unit, name _unit);
};

TRACE_2("POST INIT", _unit, _vehicleType);

[_unit, _vehicleType] call TBT_fnc_initVehicle;