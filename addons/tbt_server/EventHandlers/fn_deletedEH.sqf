#include "script_component.hpp"

/*
	Description:
		Event handler for the "Deleted" event.

	Parameters:
		Deleted EH parameters.

	Returns:
		None
*/
params ["_entity"];
private _id = _entity getVariable QGVAR(unitID);

// If it wasn't a tracked object.
if (isNil "_id") exitWith {
	ERROR_1("Tried to delete object without ID: %1", _entity);
};

private _netID = _entity call BIS_fnc_netId;
TRACE_3("Object was deleted.", _id, _entity, _netID);

// Now, here's the thing. When a unit gets inside a vehicle the deleted event is also fired,
// so we need to work around that, we can wait and see if the unit really has been deleted since
// we already got the data we need from the object. If it has really been deleted, it will be objNull
// and the isNull in the update loop.
[_entity, _id, _netID] spawn {
	sleep 1;
	params ["_entity", "_id", "_netID"];

	TRACE_3("Checking if the unit was really deleted", _entity, _id, _netID);

	if (!isNull _entity) exitWith {
		TRACE_3("False delete, entity only got inside a vehicle" _entity, _id, _netID);
	};

	LOG("It was really deleted, calling the extension.");
	//ObjectID, timestamp
	GVARMAIN(extensionName) callExtension ["entityDeleted", [
		_id,
		time
	]];
}
