#include "script_component.hpp"

/**
	Description:
		Adds the "Local" event handler to the target vehicle where it is local.
		This event handler handles the locality change of the objec by removing the other
		event handlers that need to be local (Hit, Killed and Fired) and re-adds them where the
		vehicle is local.

	Parameters:
		0 - (Object) The vehicle the event should be added to
		1 - (netID) The net ID of who has the addon.

	Returns:
		None

	Example:	
		[[_vehicle, clientOwner], TBT_fnc_addLocalEH] remoteExec ["call", owner _vehicle];
*/

private _ehCode = {
	params ["_entity", "_isLocal"];

	TRACE_2("Locality of object %1 changed, is local? %2", _entity, _isLocal);

	if (local _entity) then {
		WARNING_1("Received local change event even though the unit was already local, entity: %1", _entity);
	} else {
		// Only use this owner id because all of those should be equal anyway
		private _addonOwner = _entity getVariable QGVAR(localEventHandlerOwner);

		private _localEHID = _thisEventHandler;
		private _localEHCode = _entity getVariable QGVAR(localEventHandlerCode);

		private _firedEHID = _entity getVariable QGVAR(firedEventHandlerID);
		private _firedEHCode = _entity getVariable QGVAR(firedEventHandlerCode);

		private _hitEHID = _entity getVariable QGVAR(hitEventHandlerID);
		private _hitEHCode = _entity getVariable QGVAR(hitEventHandlerCode);

		private _killedEHID = _entity getVariable QGVAR(killedEventHandlerID);
		private _killedEHCode = _entity getVariable QGVAR(killedEventHandlerCode);

		// Fired EH is the only one that is optional
		if (!isNil "_firedEHID") then {
			// Add the event handler on where the entity is local
			_entity removeEventHandler ["Fired", _firedEHID];

			[[_entity, _addonOwner, _firedEHCode], {
				params ["_entity", "_addonOwner", "_eventHandlerCode"];

				private _id = _entity addEventHandler ["Fired", _eventHandlerCode];

				_entity setVariable [QGVAR(firedEventHandlerID), _id];
				_entity setVariable [QGVAR(firedEventHandlerOwner), _addonOwner];
				_entity setVariable [QGVAR(firedEventHandlerCode), _eventHandlerCode];
			}] remoteExec ["call", owner _entity];

			// Remove the event handler now so we don't have to check it again later
			_entity removeEventHandler ["Fired", _firedEHID];
		};

		// Do the same for the other 2 event handlers and this one too
		// Hit
		[[_entity, _addonOwner, _hitEHCode], {
				params ["_entity", "_addonOwner", "_eventHandlerCode"];

				private _id = _entity addEventHandler ["Hit", _eventHandlerCode];

				_entity setVariable [QGVAR(hitEventHandlerID), _id];
				_entity setVariable [QGVAR(hitEventHandlerOwner), _addonOwner];
				_entity setVariable [QGVAR(hitEventHandlerCode), _eventHandlerCode];
			}] remoteExec ["call", owner _entity];

			// Killed
			[[_entity, _addonOwner, _killedEHCode], {
				params ["_entity", "_addonOwner", "_eventHandlerCode"];

				private _id = _entity addEventHandler ["Killed", _eventHandlerCode];

				_entity setVariable [QGVAR(killedEventHandlerID), _id];
				_entity setVariable [QGVAR(killedEventHandlerOwner), _addonOwner];
				_entity setVariable [QGVAR(killedEventHandlerCode), _eventHandlerCode];
			}] remoteExec ["call", owner _entity];

			// Local
			[[_entity, _addonOwner, _localEHCode], {
				params ["_entity", "_addonOwner", "_eventHandlerCode"];

				private _id = _entity addEventHandler ["Local", _eventHandlerCode];

				_entity setVariable [QGVAR(localEventHandlerID), _id];
				_entity setVariable [QGVAR(localEventHandlerOwner), _addonOwner];
				_entity setVariable [QGVAR(localEventHandlerCode), _eventHandlerCode];
			}] remoteExec ["call", owner _entity];
		// Finally remove all the event handlers that still are on this vehicle on this client

		_entity removeEventHandler ["Local", _localEHID];
		_entity removeEventHandler ["Killed", _killedEHID];
		_entity removeEventHandler ["Hit", _hitEHID];
	};
};

params ["_vehicle", "_addonOwner"];

TRACE_2("Locality of object changed to me.", _vehicle, _addonOwner);

private _id = _vehicle addEventHandler ["Local", _ehCode];

_vehicle setVariable [QGVAR(localEventHandlerID), _id];
_vehicle setVariable [QGVAR(localEventHandlerOwner), _addonOwner];
_vehicle setVariable [QGVAR(localEventHandlerCode), _ehCode];