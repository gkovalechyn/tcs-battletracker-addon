#include "script_component.hpp"

/*
	Description:
		Adds a killed event handler to the given vehicle where the vehicle is local that sends back the results of the event
		to whoever called the function. It also supports ace medical by trying to get "ace_medical_lastDamageSource" object 
		if it is available.

	Parameters:
		0 - (vehicle) The object the event handler should be added to.
		1 - (netID) The ID of whoever the event should be broadcast back to.

	Returns:
		None

	Example:
		[[_vehicle, clientOwner], TBT_fnc_addKilledEH] remoteExec ["call", owner _vehicle];
*/

private _eventHandlerCode = {
	params ["_unit", "_killer", "_instigator", "_useEffects"];

	if (isNull _instigator || (_unit == _instigator)) then {
		_instigator = _unit getVariable	["ace_medical_lastDamageSource", objNull];
	};

	if (isNull _instigator) exitWith {};

	private _owner = _unit getVariable QGVAR(killedEventHandlerOwner);

	[
		_unit call BIS_fnc_netId,
		_instigator call BIS_fnc_netId
	] remoteExec ["TBT_fnc_vehicleWasKilled", _owner];
};

params ["_vehicle", "_owner"];

private _handlerID = _vehicle getVariable QGVAR(killedEventHandlerID);

if (isNil "_handlerID") then {
	private _id = _vehicle addEventHandler ["Killed", _eventHandlerCode];

	_vehicle setVariable [QGVAR(killedEventHandlerID), _id];
	_vehicle setVariable [QGVAR(killedEventHandlerOwner), _owner];
	_vehicle setVariable [QGVAR(killedEventHandlerCode), _eventHandlerCode];
};
