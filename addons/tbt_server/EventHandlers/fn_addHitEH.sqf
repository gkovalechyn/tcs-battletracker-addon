#include "script_component.hpp"

/*
	Description:
		Adds a hit event handler for the given vehicle where the vehicle is local, that event handler then sends
		the events back to the server.

	Parameters:
		0 - (vehicle) The object the event handler should be added to.
		1 - (netID) The ID of whoever the event should be broadcast back to.

	Returns:
		None
	
	Example:
		[[_vehicle, clientOwner], TBT_fnc_addHitEH] remoteExec ["call", owner _vehicle];
*/
private _eventHandlerCode = {
	params ["_unit", "_source", "_damage", "_instigator"];

	private _owner = _unit getVariable QGVAR(hitEventHandlerOwner);
	
	if (isNull _instigator) then {
		_instigator = _unit getVariable ["ace_medical_lastDamageSource", objNull];
	};

	[
		_unit call BIS_fnc_netId,
		_instigator call BIS_fnc_netId
	] remoteExec ["TBT_fnc_vehicleWasHit", _owner];
};

params ["_vehicle", "_owner"];
private _handlerID = _vehicle getVariable QGVAR(hitEventHandlerID);

if (isNil "_handlerID") then {
	_id = _vehicle addEventHandler ["Hit", _eventHandlerCode];

	_vehicle setVariable [QGVAR(hitEventHandlerID), _id];
	_vehicle setVariable [QGVAR(hitEventHandlerOwner), _owner];
	_vehicle setVariable [QGVAR(hitEventHandlerCode), _eventHandlerCode];
};
