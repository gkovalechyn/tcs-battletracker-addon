#include "script_component.hpp"

/*
	Description:
		Creates the necessary event handlers for a vehicle.
	
	Parameters:
		0 - (object) The vehicle that needs to be initialized.
		1 - (number) What kind of vehicle it is

	Returns:
		None

*/

params ["_unit", "_vehicleType"];

// Only vehicle types >= 0 are valid
if (_vehicleType < 0) exitWith {};
private _hasBeenInitialized = [_unit] call TBT_fnc_isUnitInitialized;

if (_hasBeenInitialized) exitWith {
	WARNING_2("TBT_fnc_initVehicle - Tried to initialize unit %1(%2) even though it already has been.", _unit, name _unit);
};

_unit setVariable [QGVAR(unitID), [] call TBT_fnc_getNextUnitID];

private _id = _unit getVariable QGVAR(unitID);
TRACE_2("Initializing new object", _unit, _id);

[_unit] call TBT_fnc_addEventHandlers;

private _vehicleName = "";
if (isPlayer _unit) then {
	_vehicleName = name _unit;
} else {
	_vehicleName = getText (configFile >> "CfgVehicles" >> (typeof _unit) >> "displayName");
};

// ObjectID, VehicleType,vehicleName, spawnPosition, spawnDirection, vehicleSide, isPlayer, spawnTime
GVARMAIN(extensionName) callExtension ["vehicleInit", [
	_unit getVariable QGVAR(unitID),
	_vehicleType,
	_vehicleName,
	getPos _unit,
	direction _unit,
	side _unit,
	isPlayer _unit,
	time
]];

GVAR(unitList) pushBack _unit;