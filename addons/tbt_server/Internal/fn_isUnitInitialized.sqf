#include "script_component.hpp"

/**
	Description:
		Checks if the given unit has already been initialized.

	Parameters:
		0 - (object) The unit

	Returns:
		[Boolean] Wheter the unit has already been initialized or not
 */

params ["_unit"];

private _idValue = _unit getVariable QGVAR(unitID);

!isNil "_idValue";