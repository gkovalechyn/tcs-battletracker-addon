#include "script_component.hpp"

GVARMAIN(extensionName) = "TCS-BattleTracker";

//See VehicleType enum in the extension
GVAR(UnitType_MAN) = 0;
GVAR(UnitType_HELICOPTER) = 1;
GVAR(UnitType_PLANE) = 2;
GVAR(UnitType_CAR)= 3;
GVAR(UnitType_TANK) = 4;
GVAR(UnitType_SHIP) = 5;
GVAR(UnitType_STATIC_WEAPON) = 6;

GVAR(Status_DEFAULT) = 0;
GVAR(Status_IN_VEHICLE) = 1;
GVAR(Status_UNCONSCIOUS) = 2;
GVAR(Status_DEAD) = 3;