#include "script_component.hpp"

/**
	Description:
		Called by a client telling whoever has the mod loaded that the unit was just hit.
*/
params ["_victimNetID", "_instigatorNetID"];

private _victim = _victimNetID call BIS_fnc_objectFromNetId;
private _instigator = _instigatorNetID call BIS_fnc_objectFromNetId;

if (isNull _victim)  then {
	WARNING_1("Tried to register a hit with a null victim, victim net ID %1", _victimNetID);
};

if (isNull _instigator)  then {
	WARNING_1("Tried to register a hit with a null instigator, instigator net ID %1", _instigatorNetID);
};

private _victimID = _victim getVariable QGVAR(unitID);
private _instigatorID = _instigator getVariable QGVAR(unitID);

if (isNil "_victimID") exitWith {
	WARNING_1("Tried to register a hit for a unit with no ID: %1", _victim);
};

if (isNil "_instigatorID") exitWith {
	WARNING_1("Tried to register a hit for a instigator with no ID: %1", _instigatorID);
};

TRACE_4("Vehicle was hit.", _victim, _victimID, _instigator, _instigatorID);

//InstigatorID, instigatorPos, targetID, targetPos, timestamp
GVARMAIN(extensionName) callExtension ["vehicleHit", [
	_instigatorID,
	getPos _instigator,

	_victimID,
	getPos _victim,
	time
]];

