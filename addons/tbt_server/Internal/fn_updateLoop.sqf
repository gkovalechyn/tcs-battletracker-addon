#include "script_component.hpp"

/*
	Description:
		Main update loop that keeps track of all tracked unit's positions.

	Parameters:
		None

	Returns:
		Nothing
*/

while { GVAR(isCapturing) } do {
	{
		private _unitID = _x getVariable QGVAR(unitID);

		// Units that have been deleted are no longer removed from the array
		// because when they get inside a vehicle the deleted event fires, so we
		// need to check that here and remove the deleted units.
		if (isNull _x || isNil "_unitID") then {
			GVAR(unitList) deleteAt (GVAR(unitList) find _x);
		} else {
			[_x] call TBT_fnc_sendVehicleUpdate;
		};
	} forEach GVAR(unitList);

	// Also remove the deleted units from the dead list
	{
		private _unitID = _x getVariable QGVAR(unitID);

		if (isNull _x || isNil "_unitID") then {
			GVAR(deadList) deleteAt (GVAR(deadList) find _x);
		}
	} forEach GVAR(deadList);
	
	sleep GVARMAIN(setting_captureInterval);
};