#include "script_component.hpp"

/**
	Description:
		Function called by the fired event handler when a vehicle fires a projectile.
*/
params ["_vehicleNetID", "_firedPos", "_firedTime", "_lastProjectilePos"];

private _vehicle = _vehicleNetID call BIS_fnc_objectFromNetId;
private _vehicleID = _vehicle getVariable QGVAR(unitID);

//ObjectID, FiredFromPosition, firedTimestamp, projectileHitPosition, projectileHitTime
GVARMAIN(extensionName) callExtension ["vehicleFired", [
	_vehicleID,
	_firedPos,
	_firedTime,
	_lastProjectilePos,
	time
]];