#include "script_component.hpp"
/*
	Description:
		Returns a new ID to be used for a unit;

	Parameters:
		None

	Returns:
		ID - (Number) 

*/
private _id = GVAR(nextUnitID);

INC(GVAR(nextUnitID));

_id;