#include "script_component.hpp"

/**
	Description:
		Called by the the other players to tell whoever has the mod that the player just respawned.
		Player respawns are super fucking weird, even though they are different objects the variables
		assigned to player objects are the same on the corpse and the player, so 1 variable can point to 
		multiple objects.

		It also seems everything is copied from the corpse to the new player, so if we don't clean the player
		object properly it will break player tracking.
*/
params["_corpseNetID", "_newUnitNetID"];

private _corpse = objectFromNetId _corpseNetID;
private _newUnit = objectFromNetId _newUnitNetID;

TRACE_2("Player respawned." _corpse, _newUnit);

private _oldID = _corpse getVariable QGVAR(unitID);
private _newUnitID = _newUnit getVariable QGVAR(unitID);

TRACE_2("Player respawned." _oldID, _newUnitID);

//ObjectID, timestamp
GVARMAIN(extensionName) callExtension ["entityDeleted", [
	_oldID,
	time
]];

GVAR(unitList) deleteAt (GVAR(unitList) find _corpse);
GVAR(deadList) deleteAt (GVAR(unitList) find _corpse);

[_corpse] call TBT_fnc_cleanupVehicle;
// Since everything is copied to the new unit, we also need to clean it up
// before initializing it again
[_newUnit] call TBT_fnc_cleanupVehicle;

TRACE_1("Is player?", (isPlayer _newUnit));

[_newUnit] spawn {
	params ["_newUnit"];
	
	// Wait until hopefully the old unit has been cleaned up
	sleep 1;

	[_newUnit, GVAR(UnitType_MAN)] call TBT_fnc_initVehicle;
};