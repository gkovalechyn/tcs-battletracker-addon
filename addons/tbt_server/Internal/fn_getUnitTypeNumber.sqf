#include "script_component.hpp"

/*
	Descriptions:
		Returns the VehicleType number for a unit.
		These numbers are defined in the VehicleType enum in the extension

	Parameters:
		0 - (object) The unit

	Returns:
		(number) The number representing the vehicle type this unit is. 
			Or -1 in a where it is a vehicle that cannot be captured.

*/

params ["_unit"];

if (_unit isKindOf "CAManBase") exitWith { GVAR(UnitType_MAN); };
if (_unit isKindOf "Helicopter") exitWith { GVAR(UnitType_HELICOPTER); };
if (_unit isKindOf "Plane") exitWith { GVAR(UnitType_PLANE); };
if (_unit isKindOf "Car") exitWith { GVAR(UnitType_CAR); };
if (_unit isKindOf "Tank") exitWith { GVAR(UnitType_TANK); };
if (_unit isKindOf "Ship") exitWith { GVAR(UnitType_SHIP); };
if (_unit isKindOf "StaticWeapon") exitWith { GVAR(UnitType_STATIC_WEAPON); };

-1