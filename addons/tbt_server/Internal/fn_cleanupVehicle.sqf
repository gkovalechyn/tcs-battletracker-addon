#include "script_component.hpp"

/**
	Description:
		Cleans up a vehicle or unit so that it doesn't have any dangling event handlers or variables it shouldn't have

	Parameters:
		0 - (object) The vehicle or unit that should be cleaned up
	
	Returns:
		None

*/

params ["_vehicle"];

TRACE_1("Cleaning up vehicle ", _vehicle);

private _clientCleanupCode = {
	params ["_vehicle"];

	private _firedEventHandlerID = _vehicle getVariable [QGVAR(firedEventHandlerID), -1];
	private _hitEventHandlerID = _vehicle getVariable [QGVAR(hitEventHandlerID), -1];
	private _killedEventHandlerID = _vehicle getVariable [QGVAR(killedEventHandlerID), -1];
	private _respawnEventHandlerID = _vehicle getVariable [QGVAR(respawnEventHandlerID), -1];
	private _localEventHandlerID = _vehicle getVariable [QGVAR(localEventHandlerID), -1];

	_vehicle removeEventHandler ["Fired", _firedEventHandlerID];
	_vehicle removeEventHandler ["Hit", _hitEventHandlerID];
	_vehicle removeEventHandler ["Killed", _killedEventHandlerID];
	_vehicle removeEventHandler ["Local", _localEventHandlerID];
	_vehicle removeEventHandler ["Respawn", _respawnEventHandlerID];
	// player removeEventHandler ["Respawn", _respawnEventHandlerID];

	_vehicle setVariable [QGVAR(firedEventHandlerID), nil];
	_vehicle setVariable [QGVAR(hitEventHandlerID), nil];
	_vehicle setVariable [QGVAR(killedEventHandlerID), nil];
	_vehicle setVariable [QGVAR(respawnEventHandlerID), nil];
	_vehicle setVariable [QGVAR(localEventHandlerID), nil];
	
};

[[_vehicle], _clientCleanupCode] remoteExec ["call", owner _vehicle];

private _deletedEHID = _vehicle getVariable [QGVAR(deletedEventHandlerID), -1];

_vehicle removeEventHandler ["Deleted", _deletedEHID];

_vehicle setVariable [QGVAR(deletedEventHandlerID), nil];
_vehicle setVariable [QGVAR(unitID), nil];
