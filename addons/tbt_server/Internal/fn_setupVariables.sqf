#include "script_component.hpp"

/*
	Description:
		Sets up global variables that will be used later.
*/
GVAR(nextUnitID) = 0;
GVAR(unitList) = [];
GVAR(deadList) = [];
GVAR(isCapturing) = false;

[] call TBT_fnc_constants;