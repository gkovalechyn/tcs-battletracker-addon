#include "script_component.hpp"

/**
	Description:
		Adds the necessary event handlers to a vehicle where it is local. This function
		must only be called once for the given vehicle by the addon owner on the given vehicle.

	Parameters:
		0 - (Object) The vehicle that should have the event handlers added to.

	Returns:
		None
	
	Example:
		[_vehicle] call TBT_fnc_addEventHandlers;
*/
params ["_vehicle"];

TRACE_1("Adding event handlers to: ", _vehicle);

if (GVARMAIN(setting_bulletTrackingEnabled)) then {
	[[_vehicle, clientOwner], TBT_fnc_addFiredEH] remoteExec ["call", owner _vehicle];
};

if (isPlayer _vehicle) then {
	TRACE_1("Also adding respawn event handler", "");
	[[_vehicle, clientOwner], TBT_fnc_addRespawnedEH] remoteExec ["call", owner _vehicle];
};

[[_vehicle, clientOwner], TBT_fnc_addHitEH] remoteExec ["call", owner _vehicle];
[[_vehicle, clientOwner], TBT_fnc_addKilledEH] remoteExec ["call", owner _vehicle];
[[_vehicle, clientOwner], TBT_fnc_addLocalEH] remoteExec ["call", owner _vehicle];

private _oldDeletedID = _vehicle getVariable QGVAR(deletedEventHandlerID);
if (isNil "_oldDeletedID") then {
	private _deletedEHID = _vehicle addEventHandler ["Deleted", TBT_fnc_deletedEH];
	_vehicle setVariable [QGVAR(deletedEventHandlerID), _deletedEHID];
};