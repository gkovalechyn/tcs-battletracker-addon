#include "script_component.hpp"

/**
	Description:
		RemoteExec'ed by a unit on whoever has the mod loaded to tell it the unit was killed.

	Parameters:
		0 - (netId) NetId of the victim
		1 - (netId) NetId of the killer.
*/

params ["_victimNetID", "_killerNetID"];
private _victim = _victimNetID call BIS_fnc_objectFromNetId;
private _killer = _killerNetID call BIS_fnc_objectFromNetId;

private _victimID = _victim getVariable QGVAR(unitID);
private _killerID = _killer getVariable QGVAR(unitID);

TRACE_4("Vehicle was hit.", _victim, _victimID, _killer, _killerID);

// KillerID, killerPos, targetID, targetPos, timestamp
GVARMAIN(extensionName) callExtension ["entityKilled", [
	_killerID,
	getPos _killer,

	_victimID,
	getPos _victim,

	time
]];

// Update the victim so he is marked as dead as soon as possible.
[_victim] call TBT_fnc_sendVehicleUpdate;

// Dont update it ever again
GVAR(unitList) deleteAt (GVAR(unitList) find _victim);

// But we need to track it as dead until it is deleted otherwise
// if the recording ends before it is deleted it won't appear on the recording
GVAR(deadList) pushBack _victim;