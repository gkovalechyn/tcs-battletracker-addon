#include "script_component.hpp"
/*
	Description:
		Sends the "updateVehicle" command do the extension for the given vehicle.

	Parameters:
		0 - (object) The vehicle that needs to be updated

	Returns:
		None


*/
params ["_vehicle"];

private _vehicleID = _vehicle getVariable QGVAR(unitID);
private _status = GVAR(Status_DEFAULT);
private _position = getPos _vehicle;

if (_vehicle getVariable ["ACE_isUnconscious", false]) then {
	_status = GVAR(Status_UNCONSCIOUS);
};

if (!alive _vehicle) then {
	_status = GVAR(Status_DEAD);
};

// It seems that when people are inside vehicles getPos returns the relative position to the vehicle,
// so people in vehicles don't get tracked properly
// if (!isNull objectParent _vehicle) then {
// 		private _parentPosition = getPos (objectParent _vehicle);
// 		_position = _parentPosition vectorAdd _position;
// };

//ID, Status, Position, Direction and time
GVARMAIN(extensionName) callExtension ["updateVehicle", [
	_vehicleID,
	_status,
	_position,
	getDir _vehicle,
	time
]];