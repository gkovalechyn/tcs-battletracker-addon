# BattleTracker
BattelTarcker allows recording and playback of arma missions similarly to
[OCAP](https://github.com/ocapmod/OCAP). It will generate recording files for each mission that can be played back with the [web renderer](https://gitlab.com/gkovalechyn/tcs-battletracker-webrenderer), or any other player that supports the file format.

# General use
## For server owners
What you need to do to run this mod is just add it to your server modlist and call the following functions:
* **TBT_fnc_beginCapture**
Starts the capture of the mission.  
**Parameters:**  
None
**Example:**  
```sqf
[] call TBT_fnc_beginCapture;
```

* **TBT_fnc_endCapture**
Ends the capture and writes the collected data to a file. The path where
the file will be saved to depends on which extension is being used, each extension may have it's own configuration. The mod part doesn't care about that due to separation of concerns.  
**Parameters:**  
None
**Example:**  
```sqf
[] call TBT_fnc_endCapture;
```
## !!Really important!!
* You **NEED** to call the end capture function before the mission ends. The Arma 3 `Ended` mission event does not fire on the server and therefore will not end the capture once the mission ends.
* Make sure only 1 person has the mod loaded, which in most cases would be the server. Due to how the mod is made if there is more than 1 client with the mod it will cause problems if 2 clients are recording at the same time.